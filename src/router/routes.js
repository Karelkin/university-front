
const routes = [
  {
    name: 'login',
    path: '/login',
    component: () => import('layouts/Login'),
  },
  {
    path: '/',
    component: () => import('layouts/MainLayout.vue'),
    children: [
      {
        path: '',
        component: () => import('pages/Management.vue'),
      },
      {
        name: 'profile',
        path: 'profile/:id',
        component: () => import('pages/Profile.vue'),
      },
      {
        name: 'create',
        path: 'create',
        component: () => import('pages/Create.vue'),
      },
      {
        name: 'test',
        path: 'test/:id',
        component: () => import('pages/Test.vue'),
      },
      // {
      //   path: 'management',
      //   component: () => import('pages/ManagementWrapper.vue'),
      //   children: [
      //     {
      //       name: 'management',
      //       path: '',
      //       component: () => import('pages/Management'),
      //     },
      //     {
      //       name: 'create',
      //       path: 'create',
      //       component: () => import('pages/Create.vue'),
      //     },
      //   ],
      // },
      {
        name: 'statistic',
        path: 'statistic',
        component: () => import('pages/Statistic.vue'),
      },
    ],
  },
];

// Always leave this as last one
if (process.env.MODE !== 'ssr') {
  routes.push({
    path: '*',
    component: () => import('pages/Error404.vue'),
  });
}

export default routes;
