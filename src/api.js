import axios from 'axios';

export const BASE_URL = 'http://5a37a4ea.ngrok.io/';

const API = axios.create({
  baseURL: BASE_URL,
});

export default API;
