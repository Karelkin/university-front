import API from '../../api';

const getters = {
  tests: (state) => state.tests,
  test: (state) => state.test,
  entities: (state) => state.entities,
};

const actions = {
  LOAD_TESTS({ commit }) {
    return new Promise((resolve, reject) => {
      API.get('api/tests')
        .then((response) => {
          commit('SET_TESTS', response.data.data);
          resolve(response);
        })
        .catch((error) => {
          reject(error);
        });
    });
  },
  LOAD_TEST({ commit }, id) {
    return new Promise((resolve, reject) => {
      API.get(`api/tests/${id}`)
        .then((response) => {
          commit('SET_TEST', response.data.data);
          resolve(response);
        })
        .catch((error) => {
          reject(error);
        });
    });
  },
  // eslint-disable-next-line no-empty-pattern
  CREATE_TEST({}, test) {
    return new Promise((resolve, reject) => {
      API.post('api/tests', test)
        .then((response) => {
          resolve(response);
        })
        .catch((error) => {
          reject(error);
        });
    });
  },
  // eslint-disable-next-line no-empty-pattern
  DELETE_TEST({}, id) {
    return new Promise((resolve, reject) => {
      API.delete(`api/tests/${id}`)
        .then((response) => {
          resolve(response);
        })
        .catch((error) => {
          reject(error);
        });
    });
  },
  // eslint-disable-next-line no-empty-pattern
  ADD_QUESTION({}, data) {
    return new Promise((resolve, reject) => {
      API.post(`api/tests/${data.id}/questions`, data.data)
        .then((response) => {
          resolve(response);
        })
        .catch((error) => {
          reject(error);
        });
    });
  },
  // eslint-disable-next-line no-empty-pattern
  DELETE_QUESTION({}, data) {
    return new Promise((resolve, reject) => {
      API.delete(`api/tests/${data.id}/questions/${data.id2}`)
        .then((response) => {
          resolve(response);
        })
        .catch((error) => {
          reject(error);
        });
    });
  },
  LOAD_ENTITIES({ commit }) {
    return new Promise((resolve, reject) => {
      API.get('api/tests/entities')
        .then((response) => {
          commit('SET_ENTITIES', response.data.data);
          resolve(response);
        })
        .catch((error) => {
          reject(error);
        });
    });
  },
  // eslint-disable-next-line no-empty-pattern
  UPDATE_USER({}, user) {
    return new Promise((resolve, reject) => {
      API.put(`api/employee/${user.id}`, user)
        .then((response) => {
          resolve(response);
        })
        .catch((error) => {
          reject(error);
        });
    });
  },
};

const mutations = {
  SET_TESTS(state, data) {
    state.tests = data;
  },
  SET_TEST(state, data) {
    state.test = data;
  },
  SET_ENTITIES(state, data) {
    state.entities = data;
  },
};

const state = {
  tests: [],
  test: {},
  entities: [],
};

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations,
};
