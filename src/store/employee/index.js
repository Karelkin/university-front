import API from '../../api';

const getters = {
  employee: (state) => state.employee,
  user: (state) => state.user,
};

const actions = {
  LOAD_EMPLOYEE({ commit }) {
    return new Promise((resolve, reject) => {
      API.get('api/employee')
        .then((response) => {
          commit('SET_EMPLOYEE', response.data.data);
          resolve(response);
        })
        .catch((error) => {
          reject(error);
        });
    });
  },
  LOAD_USER({ commit }, id) {
    return new Promise((resolve, reject) => {
      API.get(`api/employee/${id}`)
        .then((response) => {
          commit('SET_USER', response.data.data);
          resolve(response);
        })
        .catch((error) => {
          reject(error);
        });
    });
  },
  // eslint-disable-next-line no-empty-pattern
  CREATE_USER({}, user) {
    return new Promise((resolve, reject) => {
      API.post('api/employee', user)
        .then((response) => {
          resolve(response);
        })
        .catch((error) => {
          reject(error);
        });
    });
  },
  // eslint-disable-next-line no-empty-pattern
  DELETE_USER({}, id) {
    return new Promise((resolve, reject) => {
      API.delete(`api/employee/${id}`)
        .then((response) => {
          resolve(response);
        })
        .catch((error) => {
          reject(error);
        });
    });
  },
  // eslint-disable-next-line no-empty-pattern
  UPDATE_USER({}, user) {
    return new Promise((resolve, reject) => {
      API.put(`api/employee/${user.id}`, user)
        .then((response) => {
          resolve(response);
        })
        .catch((error) => {
          reject(error);
        });
    });
  },
};

const mutations = {
  SET_EMPLOYEE(state, data) {
    state.employee = data;
  },
  SET_USER(state, data) {
    state.user = data;
  },
};

const state = {
  employee: [],
  user: {},
};

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations,
};
