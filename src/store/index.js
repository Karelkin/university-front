import Vue from 'vue';
import Vuex from 'vuex';
import employee from './employee';
import tests from './tests';

Vue.use(Vuex);

const debug = process.env.NODE_ENV !== 'production';

export default new Vuex.Store({
  modules: {
    employee,
    tests,
  },
  strict: debug,
});
